//Functions//

//  function multiply(x, y) {
//    return x * y; }

//  function add(x, y, z) {
//      return x + y + z; }

// function divide(x) {
//   return x / 2; }

//  function sum(x, y, z) {
//      return (x + y) * z; }

//Block Scoped variables.

// let x = 1;

// if (true) {
//   let y = 5;  //Temporal dead zone until parsed
//   console.log(y); //Parsing works because of the block scope
// }
// console.log(y); //Outside scope returns error

//Hoisting - order of the 2 operations , creation and execution.
//Function declarations > variable declarations > function calls > variable assignment

// console.log(thisVar);
// var thisVar = "Hoisted"

// var thisVar
// console.log(thisVar);
// thisVar = "Hoisted"
// Works because by the time for execution var has been assigned.

// Undefined error because function call happened before variable assignment.

// console.log(thisVar);
// var thisVar = "Hoisted"

// Coercion

// const coerce = 1 + '2';
// const wut = 1 + true
// Boolean(42)
// Boolean(-42)
// String(false)
// false.toString()
// Number("")
// Number("98 88")

// const employee = new Person("6969", "Warren", "Buffet", "richguy@themoney.com");

// function Person(id, name, surname, email) {
//   this.id = id;
//   this.name = name;
//   this.surname = surname;
//   this.email = email;
//   this.contactcard = {
//     Employerid: this.id,
//     Fullname: this.name + "" + this.surname
//   };
//   Person.prototype.getFullName = function() {
//     return this.name + " " + this.surname;
//   };
// }

//Last part
// const myProject = {
//   projectName: "Becoming a developer",
//   projectType: "Education Program",
//   location: {
//     city: "Södertälje",
//     country: "Sweden",
//   },
//   length: "12 Weeks",
//   ongoing: true,
//   startDate: new Date("February 3, 2020"), 
//   deadLineDate: new Date("April 24, 2020"),
//   contributors: {
//        contributor1: "Jonah Delnero",
//        contributor2: "Elon Musk",
//        contributor3: "Dewald Els",
//     },

//  };